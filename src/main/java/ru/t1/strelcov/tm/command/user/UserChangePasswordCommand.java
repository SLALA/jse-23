package ru.t1.strelcov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.strelcov.tm.api.service.IAuthService;
import ru.t1.strelcov.tm.api.service.IUserService;
import ru.t1.strelcov.tm.util.TerminalUtil;

public final class UserChangePasswordCommand extends AbstractUserCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "user-change-password";
    }

    @NotNull
    @Override
    public String description() {
        return "Change password.";
    }

    @Override
    public void execute() {
        final IAuthService authService = serviceLocator.getAuthService();
        final IUserService userService = serviceLocator.getUserService();
        final String userId = authService.getUserId();
        System.out.println("[CHANGE PASSWORD]");
        System.out.println("ENTER NEW PASSWORD:");
        final String newPassword = TerminalUtil.nextLine();
        userService.changePasswordById(userId, newPassword);
    }

}
