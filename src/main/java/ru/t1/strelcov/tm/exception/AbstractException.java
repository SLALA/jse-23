package ru.t1.strelcov.tm.exception;

public abstract class AbstractException extends RuntimeException {

    public AbstractException() {
    }

    public AbstractException(final String message) {
        super(message);
    }

}
