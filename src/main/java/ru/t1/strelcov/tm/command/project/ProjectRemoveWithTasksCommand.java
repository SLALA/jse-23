package ru.t1.strelcov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.strelcov.tm.api.service.IProjectTaskService;
import ru.t1.strelcov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.strelcov.tm.model.Project;
import ru.t1.strelcov.tm.util.TerminalUtil;

public final class ProjectRemoveWithTasksCommand extends AbstractProjectCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "project-remove-with-tasks";
    }

    @NotNull
    @Override
    public String description() {
        return "Remove project with bound tasks.";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        final IProjectTaskService projectTaskService = serviceLocator.getProjectTaskService();
        System.out.println("[REMOVE PROJECT WITH TASKS]");
        System.out.println("ENTER PROJECT ID:");
        final String id = TerminalUtil.nextLine();
        final Project project = projectTaskService.removeProjectById(userId, id);
    }

}
