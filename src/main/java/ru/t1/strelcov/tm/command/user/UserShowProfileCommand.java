package ru.t1.strelcov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.strelcov.tm.api.service.IAuthService;
import ru.t1.strelcov.tm.model.User;

public final class UserShowProfileCommand extends AbstractUserCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "user-profile";
    }

    @NotNull
    @Override
    public String description() {
        return "Display the current user profile data.";
    }

    @Override
    public void execute() {
        final IAuthService authService = serviceLocator.getAuthService();
        System.out.println("[PROFILE]");
        final User user = authService.getUser();
        showUser(user);
    }

}
